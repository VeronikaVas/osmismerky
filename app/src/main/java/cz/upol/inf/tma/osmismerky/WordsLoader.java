package cz.upol.inf.tma.osmismerky;


import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public final class WordsLoader {
    public GameType type;

    public static List<String> loadWords(Context context) {
        List<String> words = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open("words.txt")));
            String line;
            while ((line = reader.readLine()) != null) {
                words.add(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return words;
    }

    private static GameType getGameType(String g) {
        switch (g) {
            case "Easy":
                return GameType.EASY;
            case "Medium":
                return GameType.MEDIUM;
            case "Hard":
                return GameType.HARD;
            default:
                return GameType.MEDIUM;
        }
    }

    public static String writeUsingDOM(GameType gameType, List<Word> words, List<Integer> positions, List<String> toFind, List<String> find) throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        // create root: <record>
        Element root = doc.createElement("game");
        doc.appendChild(root);

        //, List<Word> words, List<Integer> positions, List<String> wordsSearching

        Element tagGameType = doc.createElement("type");
        root.appendChild(tagGameType);
        //tagStudy.appendChild(tagTopic);
        tagGameType.setTextContent(gameType.toString());

        for (Word w : words) {
            Element tagWord = doc.createElement("word");
            root.appendChild(tagWord);
            tagWord.setAttribute("data", w.getWord());
            tagWord.setTextContent(w.getPositions().toString());
        }


        Element tagSeletedPosition = doc.createElement("selected");
        root.appendChild(tagSeletedPosition);
        //tagStudy.appendChild(tagTopic);
        tagSeletedPosition.setTextContent(positions.toString());

        for (String w : toFind) {
            Element tagWord = doc.createElement("tofind");
            root.appendChild(tagWord);

            tagWord.setTextContent(w);
        }

        for (String w : find) {
            Element tagWord = doc.createElement("find");
            root.appendChild(tagWord);

            tagWord.setTextContent(w);
        }


        // create Transformer object
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(new DOMSource(doc), result);

        // return XML string
        return writer.toString();
    }

    public static void parse(InputStream is) {
        GameType type;

        try {


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();

            NodeList nList = doc.getElementsByTagName("type");
            Node node = nList.item(0);
            System.out.println(node.getFirstChild().getNodeValue());


            NodeList wordsList = doc.getElementsByTagName("word");
            System.out.println(wordsList.getLength());
            for (int i = 0; i < wordsList.getLength(); i++) {
                Node n = wordsList.item(i).getFirstChild();
                System.out.println(wordsList.item(i).getAttributes().item(0).getNodeValue());
                System.out.println(n.getNodeValue());
            }

            System.out.println(doc.getElementsByTagName("selected").item(0).getFirstChild().getNodeValue());

            NodeList tofindList = doc.getElementsByTagName("tofind");
            for (int i = 0; i < tofindList.getLength(); i++) {
                Node n = tofindList.item(i).getFirstChild();
                //System.out.println(wordsList.item(i).getAttributes().item(0).getNodeValue());
                System.out.println(n.getNodeValue());
            }

            NodeList findList = doc.getElementsByTagName("find");
            System.out.println(findList.getLength());
            for (int i = 0; i < tofindList.getLength(); i++) {
                Node n = findList.item(i).getFirstChild();
                System.out.println(n.getNodeValue());
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
