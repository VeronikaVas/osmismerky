package cz.upol.inf.tma.osmismerky;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static android.content.ContentValues.TAG;


class GameModel {


    private GameType type;
    private String ALL_CHARS = "AÁBCČDĎEÉĚFGHIÍJKLMNŇOÓPQRŘSŠTŤUÚŮVWXYÝZŽ";

    private List<Word> words;
    private List<Word> deleted;
    private final List<String> letters = new ArrayList<>();
    private final List<Integer> occupationPositions = new ArrayList<>();
    private String selectWord = "";
    private int hint = 0;

    public GameModel(GameType type) {
        words = new ArrayList<>();
        deleted = new ArrayList<>();
        this.type = type;

    }

    public void setWords(){
        addLetters();
        for(Word w : words){
            for(int i=0; i < w.getPositions().size(); i++){
                letters.set(w.getPositions().get(i), String.valueOf(w.getWord().charAt(i)) );
            }
        }
        for(Word w : deleted){
            for(int i=0; i < w.getPositions().size(); i++){
                letters.set(w.getPositions().get(i), String.valueOf(w.getWord().charAt(i)) );
            }
        }
    }

    public void addWords(List<String> data){
        addLetters();
        for(String s : data){
            insertWord(s);
        }

    }

    protected void addLetters() {
        Random r = new Random();
        int index;
        char c;
        for (int i = 0; i < type.getSize(); i++) {
            for (int j = 0; j < type.getSize(); j++) {
                index = r.nextInt(ALL_CHARS.length() - 1);
                c = ALL_CHARS.charAt(index);
                letters.add(String.valueOf(c));
            }
        }
    }

    public int getHint() {
        int pos = -1;
        if (!words.isEmpty() && words.get(0).getWord().length() != hint) {
            System.out.println(hint);
            pos = words.get(0).getPositions().get(hint);
            if (words.get(0).getWord().length() >= hint)
                hint++;
        }

        return pos;
    }

    private int getRandomPosition() {
        return (int) (Math.random() * letters.size());
    }

    private void insertWord(String word) {
        boolean isValidPlacement = false;
        Direction direction = Direction.DIAG_UP;
        int startingPosition = 0;
        int tries = 0;

        while (!isValidPlacement && tries < 100) {
            direction = Direction.getRandom();
            startingPosition = getRandomPosition();
            isValidPlacement = isValidPosition(startingPosition, direction, word);
            tries++;
        }
        if (tries == 100) return;

        addWord(startingPosition, direction, word);
    }


    private void addWord(int start, Direction d, String word) {
        List<Integer> pos = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            letters.set(start, String.valueOf(word.charAt(i)));
            pos.add(start);
            occupationPositions.add(start);
            start = move(start, d);
        }


        words.add(new Word(word, pos));

    }

    private boolean isValidPosition(int start, Direction direction, String w) {
        if (!checkLength(start, direction, w.length())) {
            return false;
        } else if (!checkCollisions(start, direction, w)) {
            return false;
        }
        return true;
    }

    private boolean checkCollisions(int start, Direction direction, String word) {
        for (int i = 0; i < word.length(); i++) {
            if (occupationPositions.contains(start)) {
                return false;
            } else {
                start = move(start, direction);
            }
        }
        return true;
    }

    private boolean checkLength(int position, Direction direction, int length) {
        int row = position / type.getSize();
        int column = position % type.getSize();

        switch (direction) {
            case HORIZONTAL:
                return column + length <= type.getSize();
            case R_HORIZONTAL:
                return column - length >= 0;
            case VERTICAL:
                return row + length <= type.getSize();
            case R_VERTICAL:
                return row - length >= 0;

            case DIAG_DOWN:
                return row + length <= type.getSize() && column + length <= type.getSize();

            case R_DIAG_DOWN:
                return row - length >= 0 && column - length >= 0;

            case DIAG_UP:
                return row + length <= type.getSize() && column - length >= 0;

            case R_DIAG_UP:
                return row - length >= 0  && column + length <= type.getSize();

            /*;

            case R_HORIZONTAL:
                return gc - 1;

            case R_DIAG_UP:
                return new Coordinates(gc.getX() - 1, gc.getY() - 1);
            */
            default:
                return false;
        }


    }


    private int move(int gc, Direction direction) {
        switch (direction) {
            case HORIZONTAL:
                return gc + 1;
            case R_HORIZONTAL:
                return gc - 1;
            case VERTICAL:
                return gc + type.getSize();
            case R_VERTICAL:
                return gc - type.getSize();

            case DIAG_DOWN:
                return gc + type.getSize() + 1;
            case R_DIAG_DOWN:
                return gc - type.getSize() - 1;
            case DIAG_UP:
                return gc + type.getSize() - 1;
            case R_DIAG_UP:
                return gc - type.getSize() + 1;
/*
            case R_HORIZONTAL:
                return gc - 1;


            */
            default:
                return 0;
        }

    }


    /*private void addWord() {
        List<Integer> pos = new ArrayList<>();
        String word = "slovo";
        for (int i = 0; i < word.length(); i++) {
            letters.set(i, String.valueOf(word.charAt(i)));
            pos.add(i);
        }

        words.add(new Word(word, pos));

    }*/

    public List<String> getWords() {
        List<String> onlyWords = new ArrayList<>();
        for (Word w : words)
            onlyWords.add(w.getWord());
        return onlyWords;
    }

    public List<Word> getAllWords() {
        return words;
    }

    public List<Word> getDeleted() {
        return deleted;
    }

    public void setDeleted(List<Word> deleted) {
        this.deleted = deleted;
    }

    public int getWordsCount(){
        return words.size();
    }

    public String getSelectWord() {
        return selectWord;
    }

    public List<String> getLetters() {
        return letters;
    }


    public boolean matchWord(List<Integer> positions) {
        boolean success = false;
        Word word = null;
        for (Word w : words) {
            if (matchCoordinates(positions, w)) {
                success = true;
                word = w;
                break;
            }
        }

        if (success) {
            hint = 0;
            selectWord = word.getWord();
            deleted.add(word);
            words.remove(word);
        }


        return success;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    private boolean matchCoordinates(List<Integer> positions, Word w) {
        if (positions.size() != w.getPositions().size()) {
            return false;
        }

        for (int i = 0; i < positions.size(); i++) {
            if (positions.get(i) != w.getPositions().get(i)) {
                return false;
            }
        }
        return true;
    }


    public boolean endGame() {
        return words.isEmpty();
    }

}
