package cz.upol.inf.tma.osmismerky;


import java.util.List;

public class Word {
    private String word;
    private List<Integer> positions;

    public Word(String word, List<Integer> positions) {
        this.word = word;
        this.positions = positions;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<Integer> getPositions() {
        return positions;
    }

    public void setPositions(List<Integer> positions) {
        this.positions = positions;
    }
}
