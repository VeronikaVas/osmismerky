package cz.upol.inf.tma.osmismerky;

public enum Direction {

    //HORIZONTAL, VERTICAL, DIAG_UP, DIAG_DOWN, R_HORIZONTAL, R_VERTICAL, R_DIAG_UP, R_DIAG_DOWN;
    HORIZONTAL, R_HORIZONTAL,VERTICAL, R_VERTICAL, DIAG_DOWN, R_DIAG_DOWN, DIAG_UP, R_DIAG_UP;
    public static Direction getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }

}
