package cz.upol.inf.tma.osmismerky;


public enum GameType {
    EASY("Easy", 6, 6),
    MEDIUM("Medium", 9, 8),
    HARD("Hard", 10, 10);

    private String stringValue;
    private int countOfWord;
    private int size;
    private GameType(String toString, int value, int size) {
        stringValue = toString;
        countOfWord = value;
        this.size = size;
    }

    public int getCountOfWord() {
        return countOfWord;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return stringValue;
    }

}
