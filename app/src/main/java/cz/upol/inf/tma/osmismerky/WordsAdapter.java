package cz.upol.inf.tma.osmismerky;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class WordsAdapter extends BaseAdapter {
    private List<String> words;
    private List<String> selectedWords;
    private final Context mContext;

    public WordsAdapter(Context context, List<String> words) {
        this.mContext = context;
        this.words = words;
        this.selectedWords = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return words.size();
    }

    @Override
    public String getItem(int position) {
        return words.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void addSelectedWord(String word){
        selectedWords.add(word);
    }

    public List<String> getWords() {
        return words;
    }

    public List<String> getSelectedWords() {
        return selectedWords;
    }


    public void setWords(List<String> words) {
        this.words = words;
    }

    public void setSelectedWords(List<String> selectedWords) {
        this.selectedWords = selectedWords;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.word_layout, parent, false);


        TextView textView = view.findViewById(R.id.word);
        textView.setText(getItem(position));


        if (selectedWords.contains(getItem(position))) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        //textView.setInt(R.id.YourTextView, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        return view;
    }
}
