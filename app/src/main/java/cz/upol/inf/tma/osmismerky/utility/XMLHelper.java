package cz.upol.inf.tma.osmismerky.utility;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import cz.upol.inf.tma.osmismerky.GameType;
import cz.upol.inf.tma.osmismerky.Word;

public class XMLHelper {
    private GameType type;
    List<Word> words;
    List<Word> deleted;
    List<Integer> positions;
    List<String> tofind;
    List<String> find;

    public XMLHelper() {
        words = new ArrayList<>();
        deleted = new ArrayList<>();
        positions = new ArrayList<>();
        tofind = new ArrayList<>();
        find = new ArrayList<>();
    }

    public  String writeUsingDOM(GameType gameType, List<Word> words, List<Word> deleted, List<Integer> positions, List<String> toFind, List<String> find) throws Exception {
        System.out.println(positions.size());
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        // create root: <record>
        Element root = doc.createElement("game");
        doc.appendChild(root);

        //, List<Word> words, List<Integer> positions, List<String> wordsSearching

        Element tagGameType = doc.createElement("type");
        root.appendChild(tagGameType);
        //tagStudy.appendChild(tagTopic);
        tagGameType.setTextContent(gameType.toString());

        for (Word w : words) {
            Element tagWord = doc.createElement("word");
            root.appendChild(tagWord);
            tagWord.setAttribute("data", w.getWord());
            tagWord.setTextContent(w.getPositions().toString());
        }

        for (Word w : deleted) {
            Element tagWord = doc.createElement("deleted");
            root.appendChild(tagWord);
            tagWord.setAttribute("data", w.getWord());
            tagWord.setTextContent(w.getPositions().toString());
        }


        Element tagSeletedPosition = doc.createElement("selected");
        root.appendChild(tagSeletedPosition);
        //tagStudy.appendChild(tagTopic);
        tagSeletedPosition.setTextContent(positions.toString());

        for (String w : toFind) {
            Element tagWord = doc.createElement("tofind");
            root.appendChild(tagWord);

            tagWord.setTextContent(w);
        }

        for (String w : find) {
            Element tagWord = doc.createElement("find");
            root.appendChild(tagWord);

            tagWord.setTextContent(w);
        }


        // create Transformer object
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(new DOMSource(doc), result);

        // return XML string
        return writer.toString();
    }

    public  void parse(InputStream is) {
        try {


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();

            NodeList nList = doc.getElementsByTagName("type");
            Node node = nList.item(0);
            type = getGameType(node.getFirstChild().getNodeValue());


            NodeList wordsList = doc.getElementsByTagName("word");
            for (int i = 0; i < wordsList.getLength(); i++) {
                Node n = wordsList.item(i).getFirstChild();
                String pos = n.getNodeValue();
                words.add(new Word(wordsList.item(i).getAttributes().item(0).getNodeValue(),
                        getPositionFromString(pos)));
            }

            NodeList wordsList2 = doc.getElementsByTagName("deleted");
            for (int i = 0; i < wordsList2.getLength(); i++) {
                Node n = wordsList2.item(i).getFirstChild();
                String pos = n.getNodeValue();
                words.add(new Word(wordsList2.item(i).getAttributes().item(0).getNodeValue(),
                        getPositionFromString(pos)));
            }

            positions = getPositionFromString(doc.getElementsByTagName("selected").item(0).getFirstChild().getNodeValue());
            System.out.println(positions.size());

            NodeList tofindList = doc.getElementsByTagName("tofind");
            for (int i = 0; i < tofindList.getLength(); i++) {
                Node n = tofindList.item(i).getFirstChild();
                               tofind.add(n.getNodeValue());
            }

            NodeList findList = doc.getElementsByTagName("find");
            System.out.println(findList.getLength());
            for (int i = 0; i < tofindList.getLength(); i++) {
                Node n = findList.item(i).getFirstChild();
                find.add(n.getNodeValue());
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<Integer> getPositionFromString(String s){
        List<Integer> positions = new ArrayList<>();
        String s2 = s.substring(1, s.length()-1);
        String[] pos = s2.replace(" ", "").split(",");
        System.out.println("pocet:" + pos.length);
        for(String i : pos){
            positions.add(Integer.valueOf(i));
        }
        return positions;
    }

    public boolean canLoad(){
        return words.isEmpty();
    }

    public void clear(){
        words = new ArrayList<>();
        deleted = new ArrayList<>();
        positions = new ArrayList<>();
        tofind = new ArrayList<>();
        find = new ArrayList<>();
    }

    public GameType getType() {
        return type;
    }

    public List<Word> getWords() {
        return words;
    }

    public List<Integer> getPositions() {
        return positions;
    }

    public List<String> getTofind() {
        return tofind;
    }

    public List<String> getFind() {
        return find;
    }

    public List<Word> getDeleted() {
        return deleted;
    }

    private static GameType getGameType(String g) {
        switch (g) {
            case "Easy":
                return GameType.EASY;
            case "Medium":
                return GameType.MEDIUM;
            case "Hard":
                return GameType.HARD;
            default:
                return GameType.MEDIUM;
        }
    }
}
