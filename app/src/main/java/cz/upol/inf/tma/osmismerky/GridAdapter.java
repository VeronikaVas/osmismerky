package cz.upol.inf.tma.osmismerky;

import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    private List<String> values = new ArrayList<>();
    private List<Integer> highlights = new ArrayList<>();
    private List<Integer> hints = new ArrayList<>();
    private GameType type;
    //private int nbColumns = -1;

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView txtLetter;

        ViewHolder(View v) {
            super(v);
            txtLetter = v.findViewById(R.id.letter);
        }
    }

    void update(List<String> letters, GameType type) {
        values.clear();
        highlights.clear();
        values = letters;
        this.type = type;
        highlights = new ArrayList<>(Collections.nCopies(letters.size(), 0));
        hints = new ArrayList<>(Collections.nCopies(letters.size(), 0));
        notifyDataSetChanged();
    }


    void hint(int position){
        int nbHighlight = hints.get(position);
        nbHighlight++;
        hints.set(position, nbHighlight);
        notifyItemChanged(position);
    }

    void toggle(int position, boolean selected) {
        int nbHighlight = highlights.get(position);
        if (selected) nbHighlight++;
        else nbHighlight--;
        highlights.set(position, nbHighlight);
        notifyItemChanged(position);
    }

    @Override
    public GridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // inflate a new cell view and make it square
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_layout, parent, false);
        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) v.getLayoutParams();
        lp.height = parent.getMeasuredWidth() / type.getSize();
        v.setLayoutParams(lp);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // replace the contents of the view with new data
        final String letter = values.get(position);
        holder.txtLetter.setText(letter);
        if(isHighlighted(position)) {
            holder.txtLetter.setBackgroundResource(R.drawable.rounded_corner_highlighted);
        }
        else if(isHint(position)) {
            holder.txtLetter.setBackgroundResource(R.drawable.rounded_corner_hint);
        }
        else
            holder.txtLetter.setBackgroundResource(R.drawable.rounded_corner_white);

       // holder.txtLetter.setTextColor(isHighlighted(position) ? Color.WHITE : Color.BLACK);
        //holder.txtLetter.setBackgroundResource(isHighlighted(position) ? R.drawable.rounded_corner_highlighted : R.drawable.rounded_corner_white);
    }

    private boolean isHighlighted(int position) {
        return highlights.get(position) > 0;
    }
    private boolean isHint(int position) {
        return hints.get(position) > 0;
    }

    public List<Integer> getHighlights() {
        return highlights;
    }


    public void setHighlights(List<Integer> highlights) {
        this.highlights = highlights;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

}