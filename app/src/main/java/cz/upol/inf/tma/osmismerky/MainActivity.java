package cz.upol.inf.tma.osmismerky;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import cz.upol.inf.tma.osmismerky.utility.XMLHelper;

public class MainActivity extends AppCompatActivity implements GridItemTouchListener.GridItemCallbacks {
    private RecyclerView gameGrid;
    private GridLayoutManager layoutManager;
    private GridAdapter gridAdapter = null;

    private GameModel model;
    private GridItemTouchListener gridItemTouchListener;
    private GridView words;
    private WordsAdapter adapter;

    private LinearLayout remainingWordsLayout;
    private TextView count;

    private SharedPreferences SP;
    private List<String> data;
    XMLHelper xmlHelper;

    String xml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gameGrid = findViewById(R.id.grid);
        gameGrid.setHasFixedSize(true);
        gridItemTouchListener = new GridItemTouchListener(this);
        gameGrid.addOnItemTouchListener(gridItemTouchListener);

        layoutManager = new GridLayoutManager(this, 1) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        gameGrid.setLayoutManager(layoutManager);
        words = findViewById(R.id.words);
        remainingWordsLayout = findViewById(R.id.remaining_words);
        count = findViewById(R.id.count);
        SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        data = WordsLoader.loadWords(MainActivity.this);
        xmlHelper = new XMLHelper();

        playGame();
    }

    private void playGame() {
        boolean showWords = SP.getBoolean("showWords", true);
        boolean showHelp = SP.getBoolean("help", true);
        String gameType = SP.getString("gameType", "2");
        GameType type = getGameType(gameType);

        int nbColumns = type.getSize();
        model = new GameModel(type);
        model.addWords(getRandomWords(type.getCountOfWord(), type.getSize()));
        gridAdapter = new GridAdapter();
        gameGrid.setAdapter(gridAdapter);
        layoutManager.setSpanCount(nbColumns);
        gridItemTouchListener.setSpanCount(nbColumns);
        gridAdapter.update(model.getLetters(), type);

        if (!showHelp) {
            findViewById(R.id.help).setVisibility(View.INVISIBLE);
        }
        adapter = new WordsAdapter(MainActivity.this, model.getWords());
        words.setAdapter(adapter);

        if(!showWords){
            words.setVisibility(View.GONE);
            remainingWordsLayout.setVisibility(View.VISIBLE);
            count.setText(String.valueOf(model.getWordsCount()));
        }

    }

    public void hint(View v){
        int pos = model.getHint();

        if(pos != -1){
            //gridAdapter.toggle(pos, true);
            gridAdapter.hint(pos);
        }
    }

    private GameType getGameType(String g) {
        switch (g) {
            case "1":
                return GameType.EASY;
            case "2":
                return GameType.MEDIUM;
            case "3":
                return GameType.HARD;
            default:
                return GameType.MEDIUM;
        }
    }

    private void showNewGameDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage(R.string.game);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        playGame();
                    }
                });

        builder1.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private List<String> getRandomWords(int count, int size){
        List<String> words = new ArrayList<>();
        for(int i = 0; i < count; i++){
            String word = data.get((int) (Math.random() * data.size()));
            Log.e("code",word);
            if(!words.contains(word) && word.length() <= size ){

                words.add(word);
            }
        }
        return words;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_game:
                showNewGameDialog();
                break;
            case R.id.settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;

            case R.id.action_save:
                //File file = new File(getApplicationContext().getFilesDir(), "game");
                FileOutputStream outputStream;
                try {
                    xml = xmlHelper.writeUsingDOM(getGameType(SP.getString("gameType", "2")),
                            model.getAllWords(),
                            model.getDeleted(),
                            gridAdapter.getHighlights(),
                            adapter.getWords(),
                            adapter.getSelectedWords());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    outputStream = openFileOutput("game.xml", Context.MODE_PRIVATE);
                    outputStream.write(xml.getBytes());
                    outputStream.close();
                    Toast.makeText(MainActivity.this, "Uloženo!", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.action_load:
                FileInputStream fis = null;
                try {
                    fis = openFileInput("game.xml");
                    System.out.println(xml);
                    loadGame(fis);
                    Toast.makeText(MainActivity.this, "Načteno!", Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                //InputStreamReader isr = new InputStreamReader(fis);

                break;
        }
        return false;
    }


    private void loadGame(InputStream is) {
        boolean showWords = SP.getBoolean("showWords", true);
        boolean showHelp = SP.getBoolean("help", true);
        //String gameType = SP.getString("gameType", "2");
        xmlHelper.parse(is);
        GameType type = xmlHelper.getType();
        int nbColumns = type.getSize();
        model = new GameModel(type);
        model.setWords(xmlHelper.getWords());
        model.setDeleted(xmlHelper.getDeleted());
        model.setWords();


        //model.addWords(getRandomWords(type.getCountOfWord(), type.getSize()));
        gridAdapter = new GridAdapter();


        gameGrid.setAdapter(gridAdapter);
        layoutManager.setSpanCount(nbColumns);
        gridItemTouchListener.setSpanCount(nbColumns);
        gridAdapter.update(model.getLetters(), type);
        gridAdapter.setHighlights(xmlHelper.getPositions());
        gridAdapter.notifyDataSetChanged();

        adapter = new WordsAdapter(MainActivity.this, model.getWords());
        adapter.setSelectedWords(xmlHelper.getFind());
        adapter.setWords(xmlHelper.getTofind());
        adapter.notifyDataSetChanged();

        words.setAdapter(adapter);


        if (!showHelp) {
            findViewById(R.id.help).setVisibility(View.INVISIBLE);
        }


        if(!showWords){
            words.setVisibility(View.GONE);
            remainingWordsLayout.setVisibility(View.VISIBLE);
            count.setText(String.valueOf(model.getWordsCount()));
        }

        xmlHelper.clear();

    }

    @Override
    public void onWordSelected(List<Integer> positions) {
        if (model.matchWord(positions)) {
            adapter.addSelectedWord(model.getSelectWord());
            adapter.notifyDataSetChanged();
            count.setText(String.valueOf(model.getWordsCount()));
            if (model.endGame()) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        showNewGameDialog();
                    }
                }, 500);
            }
        } else {
            gridItemTouchListener.unselectSelection();
        }
    }

    @Override
    public void onLetterSelected(int position, boolean isSelected) {
        gridAdapter.toggle(position, isSelected);
    }
}
